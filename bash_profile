# load configuration from .bashrc for systems whose terminal emulators
# drop you into a login prompt (MacOS)
if [ -f ~/.bashrc ]; then
    source ~/.bashrc
fi
