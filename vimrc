" enable syntax highlighting and context-aware indenting
syntax on

" syntax-aware auto indent for python
filetype plugin indent on

" use spaces instead of tabs by default
set expandtab

" enable status bar
set ruler

" vim-plug packages
"
" Install by running :PlugInstall from inside vim or
" vim +PlugInstall from the command line. If editing this file
" you must source it first before installing plugins :so % | PlugInstall
"
" see https://github.com/junegunn/vim-plug for more info
call plug#begin()
Plug 'scrooloose/nerdtree'  " file tree browser
Plug 'kien/ctrlp.vim'       " fuzzy file finder
Plug 'morhetz/gruvbox'      " colors
Plug 'lepture/vim-jinja'    " jinja
Plug 'nvie/vim-flake8'      " PEP-8
call plug#end()

" set colors
colorscheme gruvbox

" use jinja filetype for .html files by default
au BufNewFile,BufRead *.html set ft=jinja
